//
//  Main.swift
//  Unity-iPhone
//
//  Created by 최유태 on 2017. 5. 27..
//
//
import Foundation
import UIKit
// overriding @UIApplicationMain
// http://stackoverflow.com/a/24021180/1060314
custom_unity_init(CommandLine.argc,CommandLine.unsafeArgv)
UIApplicationMain(CommandLine.argc,
                  UnsafeMutableRawPointer(CommandLine.unsafeArgv)
                    .bindMemory(
                        to: UnsafeMutablePointer<Int8>.self,
                        capacity: Int(CommandLine.argc)),
                  nil,
                  NSStringFromClass(AppDelegate.self)
)

