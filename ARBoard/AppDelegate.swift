//
//  AppDelegate.swift
//  ARBoard
//
//  Created by 최유태 on 2017. 3. 18..
//  Copyright © 2017년 YutaeChoi. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import UserNotifications

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let facebookDelegate:FBSDKApplicationDelegate = FBSDKApplicationDelegate.sharedInstance()
    var currentUnityController: UnityAppController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupColorsUIKitAppearance()
        facebookDelegate.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        currentUnityController = UnityAppController()
        currentUnityController.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        self.pushPermissionRequest(application)
        
        return true
    }
    private func setupColorsUIKitAppearance(){
        let navigationBarAppearance:UINavigationBar = UINavigationBar.appearance()
        navigationBarAppearance.tintColor = UIColor.white
        navigationBarAppearance.barTintColor = UIColor.barTintColor
        
        let navigationBarFont = UIFont.init(name: "GodoB", size: 18.0)
        if let navigationBarFont = navigationBarFont {
            navigationBarAppearance.titleTextAttributes = [
                NSForegroundColorAttributeName : UIColor.white,
                NSFontAttributeName : navigationBarFont]
        }
        let tabBarAppearance:UITabBar = UITabBar.appearance()
        tabBarAppearance.tintColor = UIColor.red
        tabBarAppearance.barTintColor = UIColor.barTintColor
        tabBarAppearance.tintColor = UIColor.redTintColor
        
        let toolBarAppearance:UIToolbar = UIToolbar.appearance()
        toolBarAppearance.barTintColor = UIColor.barTintColor
        toolBarAppearance.tintColor = UIColor.white
        
        let application:UIApplication = UIApplication.shared
        application.statusBarStyle = .lightContent
    }
    
    // MARK: Push 알람 서비스 권한 요청
    private func pushPermissionRequest(_ application: UIApplication) {
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    // MARK: - Get Token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString:String = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        KeychainService.saveDeviceToken(token: deviceTokenString)
        dump("-------Device Token-------")
        dump(deviceTokenString)
        dump("--------------------------")
    }
    
    // MARK: - Error Get Token
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // MARK: - Push notification received Data
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        switch application.applicationState {
        case .active:
            dump("Active")
            if #available(iOS 10, *) {
                EBForeNotification.handleRemoteNotification(data, soundID: 1312, isIos10: true)
            }
            else if #available(iOS 9, *) {
                EBForeNotification.handleRemoteNotification(data, soundID: 1312)
            }
        case .inactive, .background:
            dump("Inactive & Background")
            NotificationCenter.default.post(name: NotificationName.RemotePushBannerViewDidClick, object: data)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        currentUnityController.applicationWillResignActive(application)
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        currentUnityController.applicationDidEnterBackground(application)
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        currentUnityController.applicationWillEnterForeground(application)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        currentUnityController.applicationDidBecomeActive(application)
        FBSDKAppEvents.activateApp()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        currentUnityController.applicationWillTerminate(application)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return facebookDelegate.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

}

