//
//  ARBSignInViewController.swift
//  ARBoard
//
//  Created by 최유태 on 2017. 3. 22..
//  Copyright © 2017년 YutaeChoi. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import AVFoundation

class ARBSignInViewController: UIViewController {
    let dataManager:ARBDataManager = ARBDataManager.getInstance()
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var isPause: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.facebookLoginButton.delegate = self
        self.facebookLoginButton.readPermissions = ["public_profile", "email"]
        self.setupAVplayer()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.avPlayer.play()
        self.isPause = false
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.avPlayer.pause()
        self.isPause = true
    }
    /*
    fileprivate func setupFacebookLoginButton(){
        let facebookLoginButton = UIButton(type: .custom)
        facebookLoginButton.backgroundColor = UIColor.blue
        facebookLoginButton.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60)
        facebookLoginButton.center = self.view.center
        facebookLoginButton.setTitle("페이스북으로 로그인", for: .normal)
        // Handle clicks on the button
        
        // Add the button to the view
        self.view.addSubview(facebookLoginButton)
    }
     */
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    fileprivate func setupAVplayer(){
        let theURL = Bundle.main.url(forResource:"LaunchVideo", withExtension: "mov")
        
        avPlayer = AVPlayer(url: theURL!)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        avPlayer.volume = 0
        avPlayer.actionAtItemEnd = .none
        
        avPlayerLayer.frame = view.layer.bounds
        view.backgroundColor = .clear
        view.layer.insertSublayer(avPlayerLayer, at: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer.currentItem)
    }
    
    func playerItemDidReachEnd(notification: Notification) {
        let avPlayerItem: AVPlayerItem = notification.object as! AVPlayerItem
        avPlayerItem.seek(to: kCMTimeZero)
    }

}

// MARK:- Facebook Login SDK
extension ARBSignInViewController: FBSDKLoginButtonDelegate {
    // 페이스북으로 로그인
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        guard error == nil, let accessToken = FBSDKAccessToken.current(), let accessTokenString = accessToken.tokenString else {
            dump(error)
            return
        }
        dump(accessTokenString)
        dataManager.authRequest(self, domain: OAuth.Domain.facebook, token: accessTokenString) { () in
            DispatchQueue.main.async {
               self.dismiss(animated: true, completion: nil)
            }
        }
    }
    // 로그아웃
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        dump("facebook LogOut")
        self.dataManager.deleteRequest(RequestType.logout, completion: nil)
    }
}
