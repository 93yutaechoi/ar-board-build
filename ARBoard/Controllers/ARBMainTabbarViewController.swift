//
//  ARBMainTabbarViewController.swift
//  ARBoard
//
//  Created by 최유태 on 2017. 4. 14..
//  Copyright © 2017년 YutaeChoi. All rights reserved.
//

import UIKit

class ARBMainTabbarViewController: UITabBarController {
    let dataManager:ARBDataManager = ARBDataManager.getInstance()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Login Observer 등록
        NotificationCenter.default.addObserver(self, selector: #selector(showLoginViewController(notification:)), name: NotificationName.shouldShowSignInViewController, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pushBannerViewDidClick(notification:)), name: NotificationName.RemotePushBannerViewDidClick, object: nil)
    }
    
    // 로그인 Segue
    func showLoginViewController(notification: NSNotification) {
        self.performSegue(withIdentifier: SegueIdentifier.signInFromAll, sender: nil)
    }
    
    func pushBannerViewDidClick(notification: NSNotification) {
        guard self.dataManager.isCurrentUser,
            let json = notification.object as? [String:Any],
            let gameKey = json["gameKey"] as? String,
            let cookie = self.dataManager.cookie,
            let unityViewController = UnityGetGLViewController() else {
            return
        }
        
        dump("--------Push Service JSON----------")
        dump(json)
        dump("-----------------------------------")
        dump(gameKey)
        self.dataManager.gameJoinRequest(self, identifier: gameKey) { 
            UnitySendMessage("GameManager", "GetSessionID", cookie);
            self.present(unityViewController, animated: true, completion: nil)
        }
    }

    // 메모리 해제시 Observer 제거
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}
